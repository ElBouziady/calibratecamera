#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T18:55:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CalibrateCamera
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

unix {
LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui

INCLUDEPATH += '/usr/local/include'

}
